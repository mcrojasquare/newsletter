<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/newsletter-success-overlay', function () {
    return view('newsletter-success-overlay');
});

Route::get('/newsletter-success-sidebar', function () {
    return view('newsletter-success-sidebar');
});

Route::get('/newsletter-success-popup', function () {
    return view('newsletter-success-popup');
});

Route::get('/newsletter-success-footer', function () {
    return view('newsletter-success-footer');
});

Route::get('/newsletter-before-overlay', function () {
    return view('newsletter-before-overlay');
});

Route::get('/newsletter-before-sidebar', function () {
    return view('newsletter-before-sidebar');
});

Route::get('/newsletter-before-popup', function () {
    return view('newsletter-before-popup');
});

Route::get('/newsletter-before-footer', function () {
    return view('newsletter-before-footer');
});

