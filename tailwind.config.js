module.exports = {
  prefix: 'sq1m_',
  theme: {
    extend: {},
    boxShadow: {
      default: '0 5px 10px 0 rgba(0, 0, 0, 0.05)',
    },
    colors: {
      primary: {
        100: '#FFFFFF',
        200: '#F8F8F8',
        300: '#e2e8f0',
        400: '#A0AEc0',
        500: '#626262',
        600: '#718096',
        700: '#4a5568',
        800: '#2d3748',
        900: '#1A1A1A',
      },
      secondary: {
        100: '#c3dafe',
        200: '#3182ce',
        300: '#0070CD',
        400: '#2c5282',
        500: '#023171',
        600: '#4176ff',
        700: '#2b6cb0',
        800: '#63b3ed',
        900: '#2a4365',
      },
      neutral: {
        100: '#fff5f5',
        200: '#fed7d7',
        300: '#feb2b2',
        400: '#fc8181',
        500: '#f56565',
        600: '#e53e3e',
        700: '#c53030',
        800: '#9b2c2c',
        900: '#742a2a',
      },
    },
    fontFamily: {
      'sans': 'Play, sans-serif',
      'serif': 'Proxima Nova, sans-serif',
    },
    fontSize: {
      smaller: '.75rem',
      small: '.875rem',
      base: '1rem',
      big: '1.5rem'
    },
  },
  variants: {},
  plugins: [],
}