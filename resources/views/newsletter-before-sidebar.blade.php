<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Newsletter - Success</title>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body class="sq1m_h-screen sq1m_bg-primary-200">
        <div class="sq1m_container sq1m_h-full sq1m_mx-auto sq1m_flex sq1m_justify-end sq1m_items-center">
        <!-- SIDEBAR -->    
            <section class="sq1m_w-full sq1m_max-w-xs sq1m_bg-primary-100 sq1m_shadow sq1m_py-16 sq1m_px-6">
                <div class="sq1m_container sq1m_text-center">
                    <div class="sq1m_w-full sq1m_pb-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="165" height="138" viewBox="0 0 165 138" class="sq1m_mx-auto">
                            <g fill="none">
                                <path fill="#0070CD" d="M120.715.182c-7.74-.373-15.614-.31-23.088 1.525C90.152 3.542 83.044 7.318 78.79 13.11c-2.435 3.316-3.889 7.2-6.725 10.25-4.92 5.288-12.962 7.105-20.384 8.929-17.428 4.264-34.954 10.425-47.596 21.966C2.27 55.91.5 57.797.09 60.085c-.63 3.518 2.145 6.809 5.224 9.06 3.079 2.252 6.722 4.038 9 6.958 2.906 3.725 3.07 8.646 2.244 13.12-.825 4.473-2.522 8.825-2.97 13.348-.11 1.115-.136 2.278.35 3.31.645 1.367 2.08 2.295 3.485 3.076 4.984 2.78 10.362 4.797 15.952 5.98 9.63 2.052 19.674 1.667 29.534 2.51 9.86.842 20.13 3.2 27.06 9.52 4.25 3.876 8.085 9.528 14.145 9.337 3.396-.107 6.395-2.17 9.783-2.4 3.813-.259 7.26 1.804 10.855 2.98 5.107 1.673 11.02 1.51 15.808-.795 4.789-2.306 8.265-6.822 8.546-11.676.273-4.756-2.32-9.205-3.818-13.775-4.914-14.974 1.998-30.797 8.87-45.143 6.872-14.346 13.964-30.05 9.391-45.112C158.241 2.915 137.681.998 120.715.182z" opacity=".1"/>
                                <path fill="#3F3D56" d="M135.392 128.998H31.148c-7.119 0-12.89-5.771-12.89-12.89V68.35c0-1.402.577-2.742 1.594-3.706L75.09 12.281c4.586-4.347 11.772-4.347 16.358 0l52.667 49.924c2.66 2.52 4.165 6.024 4.165 9.687v44.216c0 3.419-1.358 6.697-3.775 9.115-2.417 2.417-5.696 3.775-9.114 3.775z"/>
                                <path fill="#000" d="M18.258 69.133h128.94v52.082c0 4.298-3.485 7.783-7.783 7.783H26.04c-4.298 0-7.783-3.485-7.783-7.783V69.133z" opacity=".1"/>
                                <rect width="95.892" height="95.892" x="35.188" y="28.095" fill="#0070CD" rx="10"/>
                                <path fill="#000" d="M131.08 71.287L131.08 123.987 35.188 123.987 35.188 71.287 83.134 94.461z" opacity=".1"/>
                                <path fill="#3F3D56" d="M83.134 97.44L21.7 67.747c-.772-.373-1.682-.323-2.409.133-.727.456-1.168 1.253-1.168 2.111v51.088c0 2.064.82 4.044 2.28 5.504s3.439 2.28 5.503 2.28h114.457c2.064 0 4.043-.82 5.503-2.28s2.28-3.44 2.28-5.504V70.866c0-1.046-.539-2.02-1.426-2.576-.886-.556-1.997-.618-2.94-.162L83.134 97.44z"/>
                                <path fill="#F2F2F2" d="M43.45 36.898H70.267V40.690000000000005H43.45zM41.283 50.713H120.922V52.88H41.283zM41.283 58.027H120.922V60.194H41.283zM41.283 65.341H120.922V67.508H41.283z"/>
                                <ellipse cx="83.5" cy="96" fill="#FFF" rx="17.5" ry="17"/>
                                <path fill="#57B894" d="M83.657 76.418c-11.322 0-20.5 9.178-20.5 20.5 0 11.321 9.178 20.5 20.5 20.5s20.5-9.179 20.5-20.5c-.033-11.308-9.192-20.467-20.5-20.5zm-4.15 31l-10.35-10.574 2.903-2.966 7.456 7.618 15.738-16.078 2.903 2.965-18.65 19.035z"/>
                            </g>
                        </svg>
                    </div>
                    <div class="sq1m_w-full">
                        <h1 class="sq1m_font-sans sq1m_text-big sq1m_font-bold sq1m_leading-snug sq1m_px-4 sq1m_pb-3 sq1m_text-primary-900">Exclusive offers in your inbox</h1>
                        <p class="sq1m_font-serif sq1m_text-base sq1m_leading-normal sq1m_pb-6 sq1m_text-primary-700">Register for our newsletter and never miss news announcements and special offers.</p>
                        <input class="sq1m_font-serif sq1m_bg-primary-100 focus:sq1m_outline-none focus:sq1m_shadow-outline sq1m_border sq1m_border-primary-400 sq1m_text-primary-500 sq1m_placeholder-primary-500 sq1m_rounded sq1m_py-3 sq1m_px-4 sq1m_block sq1m_w-full sq1m_appearance-none sq1m_leading-normal sq1m_mb-6" type="email" placeholder="Enter your email address here">
                        <button class="sq1m_font-serif sq1m_text-base sq1m_font-bold sq1m_w-full sq1m_p-3 sq1m_rounded sq1m_bg-secondary-200 sq1m_text-primary-100">
                            Subscribe
                        </button>
                    </div>
                </div>
            </section>
        <!-- END: SIDEBAR -->
        </div>
    </body>
</html>
